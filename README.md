# P2P orderbook

This is an example of a p2p orderbook. 

The **user.yml** is **mandatory** and includes important user profile informations. 

If you want **create an order** then you must **create** a **yml file** with custom name or number. In this example I have named it **aln-double-bed.yml** to make it easier to find the order later. Use the form at https://p2p.faircoin.co/en/form.html to create or edit the yml code.

If you have done all changes then add your git url to import queue ( https://p2p.faircoin.co/en/import.php ). You can find the git url behind the "clone" button. The url should be similar like this ( git@git.fairkom.net:faircoin.co/p2p-orderbook.git )
